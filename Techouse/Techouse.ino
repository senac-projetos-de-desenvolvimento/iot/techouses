#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <ESPmDNS.h>

#define pinPIR 2

// Informaçoes da rede
const char *ssid = "Error 404 not found";//nome da rede wifi
const char *password = "@M200698";//senha da rede wifi

//Define quais portas irão controla as lampadas

int ledQuartoA = 21; 
int ledQuartoB = 22;
int ledSala = 19;
int ledBanheiro = 23;
int ledEscritorio = 5;
int ledCorredor = 17;
int ledCozinha = 4;

WebServer server(80); // Porta 80
 

void setup(void)
{
// Define os pinos como Saida
pinMode(ledQuartoA, OUTPUT);
pinMode(ledQuartoB, OUTPUT);
pinMode(ledSala, OUTPUT);
pinMode(ledBanheiro, OUTPUT);
pinMode(ledEscritorio, OUTPUT);
pinMode(ledCorredor, OUTPUT);
pinMode(ledCozinha, OUTPUT);

//define o pinPIR como entrada
pinMode(pinPIR, INPUT);

  delay(1000);
  Serial.begin(115200);
  WiFi.begin(ssid, password); //Iniciar ligação à rede Wi-Fi
  Serial.println("");
 
  // Aguardar por ligação
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print("Conectando");
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Conectado em: ");
  Serial.println(ssid);
  Serial.print("Endereço IP: ");
  Serial.println(WiFi.localIP());
 
  server.on("/", []() {
    server.send(200, "text/html", page);
  });
  
  //                Quarto A
  
  server.on("/ledQuartoAOn", []() {
    server.send(200, "text/html", page);
    digitalWrite(ledQuartoA, HIGH); // Liga o LED
    Serial.println("A lâmpada A do Quarto esta acesa");
    delay(1000);
  });
  server.on("/ledQuartoAOff", []() {
    server.send(200, "text/html", page);
    digitalWrite(ledQuartoA, LOW); // Desliga o LED
     Serial.println("A lâmpada A do Quarto esta desligada");
    delay(1000);
  });
  //                Quarto b
  
  server.on("/ledQuartoBOn", []() {
    server.send(200, "text/html", page);
    digitalWrite(ledQuartoB, HIGH); // Liga o LED
    Serial.println("A lâmpada B do Quarto esta acesa");
    delay(1000);
  });
  server.on("/ledQuartoBOff", []() {
    server.send(200, "text/html", page);
    digitalWrite(ledQuartoB, LOW); // Desliga o LED
    Serial.println("A lâmpada B do Quarto esta desligada");
    delay(1000);
  });
    //                os dois quartos
  
  server.on("/ledQuartoAEBOn", []() {
    server.send(200, "text/html", page);
    digitalWrite(ledQuartoA, HIGH); 
    digitalWrite(ledQuartoB, HIGH); 
    Serial.println("As lâmpadas A e B do Quarto estão acesas");
    delay(1000);
  });
  server.on("/ledQuartoAEBOff", []() {
    server.send(200, "text/html", page);
    digitalWrite(ledQuartoA, LOW);
    digitalWrite(ledQuartoB, LOW); 
    Serial.println("As lâmpadas A e B do Quarto estão acesas");
    delay(1000);
  });
  
  //                Sala
  
  server.on("/ledSalaOn", []() {
    server.send(200, "text/html", page);
    digitalWrite(ledSala, HIGH); // Liga o LED
    Serial.println("A lâmpada da sala esta acesa");
    delay(1000);
  });
  server.on("/ledSalaOff", []() {
    server.send(200, "text/html", page);
    digitalWrite(ledSala, LOW); // Desliga o LED
    Serial.println("A lâmpada da sala esta desligada");
    delay(1000);
  });
    //                Banheiro
  
  server.on("/ledBanheiroOn", []() {
    server.send(200, "text/html", page);
    digitalWrite(ledBanheiro, HIGH); // Liga o LED
    Serial.println("A lâmpada do banheiro esta acesa");
    delay(1000);
  });
  server.on("/ledBanheiroOff", []() {
    server.send(200, "text/html", page);
    digitalWrite(ledBanheiro, LOW); // Desliga o LED
    Serial.println("A lâmpada do banheiro esta desligada");
    delay(1000);
  });
      //                Escritorio
  
  server.on("/ledEscritorioOn", []() {
    server.send(200, "text/html", page);
    digitalWrite(ledEscritorio, HIGH); // Liga o LED
    Serial.println("A lâmpada do escritorio esta acesa");
    delay(1000);
  });
  server.on("/ledEscritorioOff", []() {
    server.send(200, "text/html", page);
    digitalWrite(ledEscritorio, LOW); // Desliga o LED
    Serial.println("A lâmpada do escritorio esta desligada");
    delay(1000);
  });
        //                Corredor
  
  server.on("/ledCorredorOn", []() {
    server.send(200, "text/html", page);
    digitalWrite(ledCorredor, HIGH); // Liga o LED
    delay(1000);
  });
  server.on("/ledCorredorOff", []() {
    server.send(200, "text/html", page);
    digitalWrite(ledCorredor, LOW); // Desliga o LED
    delay(1000);
  });
  //                Cozinha
  
  server.on("/ledCozinhaOn", []() {
    server.send(200, "text/html", page);
    digitalWrite(ledCozinha, HIGH); // Liga o LED
    Serial.println("A lampada da cozinha esta acesa");
    delay(1000);
  });
  server.on("/ledCozinhaOff", []() {
    server.send(200, "text/html", page);
    digitalWrite(ledCozinha, LOW); // Desliga o LED
    Serial.println("A lampada da cozinha esta desligada");
    delay(1000);
  });
  
  server.begin();
  Serial.println("Web server inicializado");
  
  // inicia o sistema com as lampadas desligadas
  
  digitalWrite(ledQuartoA, LOW);
  digitalWrite(ledQuartoB, LOW);
  digitalWrite(ledSala, LOW);
  digitalWrite(ledBanheiro, LOW);
  digitalWrite(ledEscritorio, LOW);
  digitalWrite(ledCorredor, LOW);
  digitalWrite(ledCozinha, LOW);
  
}
  
 
void loop(void)
{
    bool valorPIR = digitalRead(pinPIR);

  if (valorPIR) {
    Serial.println("A lampada do corredor esta ligada");
    digitalWrite(ledCorredor, HIGH);
  } else {
    Serial.println("A lampada do corredor esta desligada ");
     digitalWrite(ledCorredor, LOW);
  }
  server.handleClient();
}
