Techouse

A TecHouse é um projeto de SmartHouse (Casa inteligente) com a proposta de dar ao usuario controle total da iluminação da sua casa, proporcionando um controle remoto das luzes e com isso ter um controle melhor de Consumo de energia, proporcionando ao usuario um painel de controle onde podera ver a quanto tempo as lampadas estão ou não acessas.

Instalação

- Baixe o apk no seu celular 
- instale o aplicativo

Para ligar os LEDs serão necessarios os seguintes equipamentos.

- 1 esp32
- 1 Protoboard
- leds
- Cabos  Wire Jumper
- resistores

Requisitos
- Ide Do arduino instalada

Instalação
- Baixe o arquivo Techouse.ino
- atualize as biliotecas necessarias
*Observação - Dependendo do seu aparelho pode ser necessario outras bibliotecas, nesse caso consultar a documentação especifica do seu aparelho
O que eu usei nesse exemplo foi o ESP32 LILYGO® TTGO T-Display.
- Atualize a URL Para gerenciadores de placas, no menu de preferencias da IDE, adicione este link https://dl.espressif.com/dl/package_esp32_index.json
- Apos isso, podera baixar a bibliotecas do esp32, no menu gerenciar bibliotecas 
- Upe o Codigo pro seu Esp32

Montagem do circuito
- conecta o esp32 na protoboard
- Conectar o fio positivo nas portas do ESP32 conforme indicado pelo codigo, no arquivo arduino.ino.
- Conectar o fio negativo na porta GND do ESP32, usando a protoboard.
- Conectar a outra extremidade do fio positivo e negativo do led.
Caso não saiba qual é extremidade correta, A haste (perna) maior do LED é o lado positivo e a menor é o lado negativo.
- Agora colocar um resistor (nesse projeto foi utilizado resitor de 220 Ohms ), O resitor deve ser conectada na porta 5V do ESP32 e a outra extremidade na perna negativa do led.


 
